# ***************************************************************************
 # *  Copyright(C) 2020 Michal Charvát
 # *
 # *  This program is free software: you can redistribute it and/or modify
 # *  it under the terms of the GNU General Public License as published by
 # *  the Free Software Foundation, either version 3 of the License, or
 # *  (at your option) any later version.
 # *
 # *  This program is distributed in the hope that it will be useful,
 # *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 # *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 # *  GNU General Public License for more details.
 # *
 # *  You should have received a copy of the GNU General Public License
 # *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 # *  ***************************************************************************

import cv2
import signal
import datetime
import numpy as np
from contextlib import contextmanager


def Normalize_lin8(image: np.ndarray, min_clip_value: int = 0, max_clip_value: int = 65535) -> np.ndarray:
    image = np.clip(image, min_clip_value, max_clip_value)
    min_val = np.amin(image)
    max_val = np.amax(image)
    normalized_image = np.interp(image, [min_val, max_val], [0, 255]).astype('uint8')
    return normalized_image

def Normalize_clahe(image: np.ndarray, min_clip_value: int = 0, max_clip_value: int = 65535) -> np.ndarray:
    clahe = cv2.createCLAHE(clipLimit=3.0, tileGridSize=(3, 3))
    image_clipped = np.clip(image, min_clip_value, max_clip_value)
    image_normalized = clahe.apply(image_clipped)
    return image_normalized


class FpsCounter(object):
    def __init__(self, report_time_ms: int, report_callback=lambda fps: print('FPS: {}'.format(fps))):
        self._frame_counter = 0
        self._report_time_sec = report_time_ms / 1000.0
        self._report_callback = report_callback
        self._start_time = datetime.datetime.now()

    def tick(self):
        self._frame_counter += 1
        now = datetime.datetime.now()
        elapsed = now - self._start_time
        elapsed_seconds = elapsed.seconds + elapsed.microseconds / 1000000.0

        if elapsed_seconds >= self._report_time_sec:
            fps = self._frame_counter / elapsed_seconds
            self._report_callback(fps)
            self._frame_counter = 0
            self._start_time = now


@contextmanager
def Timeout(seconds):
    def raise_timeout(signum, frame):
        raise TimeoutError

    signal.signal(signal.SIGALRM, raise_timeout)
    signal.alarm(seconds)

    try:
        yield
    except TimeoutError:
        raise
    finally:
        signal.signal(signal.SIGALRM, signal.SIG_IGN)