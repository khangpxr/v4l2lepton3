# v4l2lepton3 library
This library has been reworked as a part of a Masters's thesis with topic: __System for People Detection and Localization Using Thermal Imaging Cameras__ (2020),
which will be published later this year.


The first version of the library released as a part of Bachelor's thesis with the topic: __Detection of People in Room Using Low-Cost Thermal Imaging Camera__ can be found in the `bp` branch of the repository.
And the thesis itself and its second part can be found [here](https://gitlab.com/CharvN/thermo-person-detection).

`v4l2lepton3` is a pack of tools that allows to interact with the [Lepton 3.5](https://www.flir.com/globalassets/imported-assets/document/flir-lepton-engineering-datasheet.pdf) (should work also for version 3.0).
More specifically, to control the camera over CCI (I2C), capture and save individual frames, stream live thermal video over TCP connection and redirect it into a virtual video device on the client side.
The repository contains the following executables:
- `lepton3server` - reads raw frames (Y16) from Lepton 3.5 via SPI and streams them over TCP
- `lepton3client` - connects to the server, read frames over TCP and pushes them into a virtual v4l2loopback video device
- `lepton3client.py` - connects to the server, reads frames over TCP and displays live video in an OpenCV window
- `lepton3control.py` - controls the camera over its CCI (I2C) interface
- `lepton3capture.py` - allows for capturing a single frame at a time locally (over SPI) and remotly (from a `lepton3server`)

# Installation
#### v4l2lepton3 C++ server & client executables
To build `v4l2lepton3` C++ executables `server` and `client` for video capturing use CMakeLists.txt. For example like this:
```shell script
mkdir build && cd build
cmake ..
make all
```
In order to build only the server or only the client, use `make server` or `make client` respectively.


Compilation requires the `boost c++` library to be installed (`openmp` is optional for client speedup):
```shell script
sudo apt install libboost-dev libomp-dev
```


The `lepton3client` requires an installed and initialized virtual video loopback device `v4l2loopback`. Follow installation guidelines in the official repository linked in References)
____
#### v4l2lepton3 Python3 package
The v4l2lepton3 Python library contains implementations of the following classes:
- `Lepton3Client` -- connects to the `lepton3server` over TCP and receives raw frames.
- `Lepton3Capturer` -- allows for single frame capture at a time locally (over SPI) or remotely (from the `lepton3server`). Can be used for testing or with scheduling with `cron` to get a single frame at a particular moment.
- `Lepton3Control` -- issues commands directly to the camera over I2C.

These classes can be imported and used to communicate with the camera. Usage examples can be found in `lepton3client.py`, `lepton3control.py` and `lepton3capture.py`.

In order to be able to run the Python3 control and capture scripts `lepton3capture.py`, `lepton3control.py` and `lepton3client.py`,
the following packages must be installed: `OpenCV`, `numpy`. The `lepton3control.py` requires also the `smbus2` package for I2C. The `lepton3capture.py` script for local capturing (using SPI) requires the `SPI-Py` module.

```shell script
sudo apt install python3-opencv
python3 -m pip install smbus2 numpy
python3 -m pip install git+git://github.com/lthiery/SPI-Py.git
```

The package is required for some tools in the second part of the project (repository [thermo-person-detection](https://gitlab.com/CharvN/thermo-person-detection)).
In order to use tools in the other repository that interact with the camera directly or through the v4l2lepton3 library (Python or C++),
the `v4l2lepton3` Python3 package must be installed in the system using the following command:

```shell script
python3 -m pip install .
```

In order to use scripts `on` and `off` that were designed to be executed on a Raspberry Pi with the Lepton camera connected to the 
virtual GPIO pin 15 (wPi), or execute scripts `v4l2loopback_preview.py`, `lepton3capture.py`, `lepton3client.py` or `lepton3control.py`, you need to add execute permissions:

```shell script
chmod +x lepton3client.py lepton3control.py lepton3capture.py v4l2loopback_preview.py on off
```


# Usage
## lepton3server (C++)
Designed to run on a Raspberry-Pi-like device with an SPI interface connected to the Lepton 3.5 camera.
The server opens a TCP socket and waits for a client to connect. Once a client connects, the server opens the SPI device and starts
pulling frames from the camera. It is using double segment and double frame buffering to prevent losing sync with the camera.
Once a previous frame has been sent out, it waits for the next available frame and starts the next transmission.
A dedicated thread pulling frames over SPI does not wait for the TCP transmission to finish. If the transmission is not ready,
the frame is dropped and the thread continues to pull a next frame to avoid losing sync.

Optionally, a `zlib` compression may be turned on, which effectively reduces the network usage from ~360 KB/s to ~140-170 KB/s
for the cost of occasional lost of sync with the camera. When that happens, the server quickly recovers, resynchronizes with the camera and starts sending once again.

```shell script
./lepton3server [-p <port>] [-s <spi_device>] [-t <timeout_ms>] [-h] [-c]
```
-   `-h` `--help`  -- shows help
-   `-p` `--port` `<port>` -- sets server TCP port, default `2222`
-   `-s` `--spi` `<spi_device>` -- SPI device, default `/dev/spidev0.0`
-   `-t` `--timeout` `<timeout_ms>` -- frame acquisition timeout in ms, default `5000`
-   `-c` `--compress` -- turns on zlib compression

## lepton3client (C++)
Designed to run on the device with a virual video device installed (`v4l2loopback`). The `lepton3client` connects to the `server` via ipv4:port
and starts receiveng video frames. When the connection is established, the `lepton3client` opens a virtual video device and starts writing the frames into it.

This effectively connects the Lepton 3.5 thermal module to a standardized video device used on linux machines. (e.g. for webcam capture)
This enables us to view a live feed from the module, stream the video, perform various standard image processing routines using standard video tools like `ffmpeg`, `gstream` etc.

The frames transmitted over TCP are in RAW format (Y16), so the camera must be set to output RAW. The client can either setup the virtual video device as `Y16` format (160x120x1x16bit) and write the frames as is,
 or set the video device to `RGB24` (160x120x3x8bit) and perform linear normalization before sending the frame into the virtual video device.


```shell script
lepton3client -i <ipv4> [-p <port>] [-v <video_dev>] [-c] [-r]
```
-   `-h` `--help` -- shows help
-   `-i` `--ip` `<ipv4>` -- ip address of the server
-   `-p` `--port` `<port>` -- port of the server, default `2222`
-   `-v` `--video` `<video_dev>` -- name of a video loopback device, default `/dev/video0`
-   `-c` `--compressed` -- turns on frame decompression
-   `-r` `--raw` -- sets the video device format to `Y16`, default is `RGB24` + linear normalization

#### Errors
If you get the error `22` when getting or setting format of the v4l2loopback using `ioctl(VIDIOC_G_FMT)` (observed on ubuntu 18.04 v4l2loopback-dkms 0.10)
Try upgrading to `v4l2loopback-dkms 0.12.5` (or newer) using:
```shell script
sudo modprobe -r v4l2loopback
sudo apt remove v4l2loopback-dkms
wget http://deb.debian.org/debian/pool/main/v/v4l2loopback/v4l2loopback-dkms_0.12.5-1_all.deb
sudo dpkg -i v4l2loopback-dkms_0.12.5-1_all.deb
rm v4l2loopback-dkms_0.12.5-1_all.deb
```
___
If you have the problem that the video device is in `/dev/videoX` , the `lepton3client` runs without errors, it is possible to obtain a frame from the device using OpenCV (VideoCapture),
but the device is not visible in the browser, zoom, cheese and other regular apps as a webcam, than use the `exclusive_caps=1` arguement:
```shell script
sudo modprobe v4l2loopback exclusive_caps=1 video_nr=5
``` 


## lepton3client.py (python3)
Importing the `Lepton3Client` class can serve as a base for custom frame processing applications.
It can connect and receive frames from the `lepton3server` (c++) application running on the device with Lepton 3.5 connected to it.
The script can also be interpreted with Python3. In that case, the received frames are normalized using CLAHE and displayed in real time
 on screen using `OpenCV`.

```shell script
./lepton3client.py <ip> <port>
```
- `<ip>` -- required, e.g. 192.168.1.2
- `<port>` -- optional, default `2222`

If you want to use the Python3 client implementation in your own project, install the `v4l2lepton3` Python3 package
using
```shell script
python3 -m pip install .
```
and use it in the following way:
```python
from v4l2lepton3 import Lepton3Client

client = Lepton3Client(ip, port)
with client:
    while True:
        frame = client.get_frame()
        # process the RAW frame (uint16 np.ndarray 160x120)
```


## lepton3control.py (python3)
Script that controls the Lepton 3.5 camera over _Command and Control Interface_ (CCI) using the I2C interface.

```
./lepton3control.py <i2c_number> <command> <method> [<data>]
```
- `<i2c_number>` -- for `/dev/i2c-1` use `1`
- `<command>` -- one of the commands below
- `<method>` -- one of the `get`, `set`, `run` - see available methods next to commands below
- `<data>` -- parameter for the `set` method only, leave empty to see available options

**Available commands**:
- `agc_calc_enable` -- [`get`, `set`]
- `agc_enable` -- [`get`, `set`]
- `agc_policy` -- [`get`, `set`]
- `oem_bad_pixel_replacement_enable` -- [`get`, `set`]
- `oem_calc_status` -- [`get`]
- `oem_customer_part_number` -- [`get`]
- `oem_flir_part_number` -- [`get`]
- `oem_output_format` -- [`get`, `set`]
- `oem_reboot` -- [`run`]
- `oem_sw_revision` -- [`get`]
- `oem_temporal_filter_enable` -- [`get`, `set`]
- `oem_thermal_shutdown_enable` -- [`get`, `set`]
- `oem_video_out_enable` -- [`get`, `set`]
- `rad_enable` -- [`get`, `set`]
- `rad_ffc_run` -- [`run`]
- `rad_tlinear_auto_scale` -- [`get`, `set`]
- `rad_tlinear_enable` -- [`get`, `set`]
- `rad_tlinear_scale` -- [`get`, `set`]
- `rad_tshutter_mode` -- [`get`, `set`]
- `sys_aux_temp_k` -- [`get`]
- `sys_camera_up_time` -- [`get`]
- `sys_customer_serial_number` -- [`get`]
- `sys_ffc_run` -- [`run`]
- `sys_ffc_status` -- [`get`]
- `sys_flir_serial_number` -- [`get`]
- `sys_fpa_temp_k` -- [`get`]
- `sys_frames_to_average` -- [`get`, `set`]
- `sys_frames_to_average_run` -- [`run`]
- `sys_gain_mode` -- [`get`, `set`]
- `sys_ping` -- [`run`]
- `sys_scene_statistics` -- [`get`]
- `sys_shutter_position` -- [`get`, `set`]
- `sys_telemetry_enable` -- [`get`, `set`]
- `sys_telemetry_location` -- [`get`, `set`]
- `vid_focus_calc_enable` -- [`get`, `set`]
- `vid_freeze_enable` -- [`get`, `set`]
- `vid_low_gain_pcolor_lut` -- [`get`, `set`]
- `vid_output_format` -- [`get`, `set`]
- `vid_pcolor_lut` -- [`get`, `set`]

The `Lepton3Control` class is also available in the Python package and can be used in the following way:

```python
from v4l2lepton3.control import Lepton3Control

lepton = Lepton3Control(1) # 1 for /dev/i2c-1
print('Booted: {} Ready: {} ErrorCode: {}'.format(*lepton.get_status()))
result, error_code = lepton.execute_command(command_name, method_name, parameter)
# for example: 
# lepton.execute_command('vid_pcolor_lut', 'get', None)
# lepton.execute_command('rad_enable', 'set', 'on')

print('Error code: {}'.format(error_code))
print('Done.' if result is None else 'Result: {}'.format(result))
```

## lepton3capture.py (python3)
This script can be used for single frame capture scenarios. It can capture and save one frame per run, which can be used with scheduling, or unlimited amount with a specified delay.
The frame is saved in both raw (Y16) `.tiff` and regular normalized grayscale `.png`. Capturing can work both locally (using an SPI device in the local device tree) or remotely (connecting to the `lepton3server` over TCP).
These test scripts are meant to work locally (with the camera directly connected) and allow to capture a single frame from the Lepton camera at a time. The script can be used in the following way:

```shell script
./lepton3capture.py [-h] [-s SPI] [-i IP] [-p PORT] [-d DIRECTORY] [-r DELAY] [-t TIMEOUT]
```
- `-s SPI` `--spi SPI` -- SPI device with the local Lepton 3.5 camera, e.g. /dev/spidev0.0
- `-i IP` `--ip IP` -- IP of the remote v4l2lepton3 server running for remote capture.
- `-p PORT` `--port PORT` -- port of the remote v4l2lepton3 server. Default `2222`
- `-d DIRECTORY` `--directory DIRECTORY` -- Directory into which the frames should be saved. Default `.`
- `-r DELAY` `--repeat DELAY` -- Repeated capturing after `DELAY` seconds until SIGINT.
- `-t TIMEOUT` `--timeout TIMEOUT` -- Timout to get a frame. If it is reached, the script exits with 2. Default `45`

__Note__. The script must be executed exclusively with one of `-s` or `-i` parameter. If the `-r` parameter is used, the script does not quit but saves one frame every `DELAY` seconds.

The one frame per run can be scheduled using the `cron` scheduling tool like:
```shell script
crontab -e
```
and for local capturing into the `/home/pi/data` directory every minute between 10 and 18 hours insert the following line:
```text
* 10-18 * * * python3 /home/pi/v4l2lepton3/lepton3captury.py -s /dev/spidev0.0 -d /home/pi/data
```

For a batch capture into the local `data` folder every 5 seconds remotely, run:
```shell script
./lepton3capture.py -i 192.168.1.99 -d ./data -r 5
```

The `Lepton3Capturer` implementation, which takes care about the local capture using SPI directly, is also available in the Python package and can be used for example like:
```python
from v4l2lepton3.capture import Lepton3Capturer

capturer = Lepton3Capturer('/dev/spidev0.0')

while(True):
    try:
        with capturer:
            frame = capturer.get_frame(timeout=15) # 15 s
            # process the RAW frame (uint16 np.ndarray 160x120)

    except KeyboardInterrupt:
        break
    except TimeoutError:
        exit(2)
    except Exception as e:
        exit(3)
```


## Enviroment
This tool has been developed and tested on a RaspberryPi 3B+ running Raspbian GNU/Linux 10 (buster) Lite with the 4.19.97-v7+ kernel on the server side and Ubuntu 5.3.0-59.53~18.04.1-generic 5.3.18 


## References
- v4l2loopback [repository](https://github.com/umlaeute/v4l2loopback)
- v4l2lepton - version for older lepton module [repository](https://github.com/groupgets/LeptonModule/tree/master/software/v4l2lepton)
- Lepton 3.5 Software Interface Description [Document](https://www.flir.com/globalassets/imported-assets/document/flir-lepton-software-interface-description-document.pdf)
- smbus2 GitHub [repository](https://github.com/kplindegaard/smbus2)
- SPI-Py git [repository](https://github.com/lthiery/SPI-Py)