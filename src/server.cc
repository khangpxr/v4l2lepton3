/* ***************************************************************************
 *  Copyright(C) 2020 Michal Charvát
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *  ***************************************************************************
 */

#include "log.hh"
#include "lepton3.hh"
#include "opt_parser.hh"

#include <string>
#include <csignal>
#include <boost/asio.hpp>
#include <boost/iostreams/filter/zlib.hpp>
#include <boost/iostreams/filtering_stream.hpp>

namespace io = boost::iostreams;
namespace asio = boost::asio;
using boost::asio::ip::tcp;
using namespace std;

void SignalHandler(int signum) { BOOST_LOG_TRIVIAL(info) << "Received SIGINT. Exiting."; exit(signum); }

const static char Help[] = "Usage\nserver [-p <port>] [-s <spi_device>] [-t <timeout_ms>] [-h] [-c]\n"
                           "   -h --help  -- shows help \n"
                           "   -p --port <port> -- sets server port, default `2222`\n"
                           "   -s --spi <spi_device> -- SPI device, default `/dev/spidev0.0`\n"
                           "   -t --timeout <timeout_ms> -- frame acquisition timeout in ms, default `5000`\n"
						   "   -c --compress -- turns on zlib compression";

int main(int argc, const char* argv[])
{
	init_logging("server.%N.log");
	signal(SIGINT, SignalHandler);

	OptParser arguments;
	arguments.RegisterOption("-h", "--help", true);
	arguments.RegisterOption("-p", "--port", true, Option::ParamType::RequiredParam, "2222");
	arguments.RegisterOption("-s", "--spi", true, Option::ParamType::RequiredParam, "/dev/spidev0.0");
	arguments.RegisterOption("-t", "--timeout", true, Option::ParamType::RequiredParam, "5000");
	arguments.RegisterOption("-c", "--compress");
	arguments.ParseArguments(argc, argv);

	if (arguments.GetOption("-h")->entered)
	{
		BOOST_LOG_TRIVIAL(info) << Help;
		return 0;
	}
	if (!arguments.Valid())
	{
		BOOST_LOG_TRIVIAL(fatal) << "Invalid arguments.";
		BOOST_LOG_TRIVIAL(info) << Help;
		return 1;
	}

	try
	{
		uint16_t port = stoi(arguments.GetOption("-p")->param);
		bool compressionOn = arguments.GetOption("-c")->entered;
		int frameAcquisitionTimeout = stoi(arguments.GetOption("-t")->param);

		boost::asio::io_service io_service;
		tcp::acceptor acceptor(io_service, tcp::endpoint(tcp::v4(), port));

		while (true)
		{
			tcp::socket socket(io_service);
			BOOST_LOG_TRIVIAL(info) << "Waiting for a client on port: " << port;
			acceptor.accept(socket);
			BOOST_LOG_TRIVIAL(info) << "Client " << socket.remote_endpoint().address().to_string() << ":" << socket.remote_endpoint().port() << " connected.";

			asio::streambuf compressedStreamBuffer;
			io::filtering_ostream compressingStream;
			if(compressionOn) compressingStream.push(io::zlib_compressor(io::zlib_params(io::zlib::best_speed)));
			compressingStream.push(compressedStreamBuffer);

			Lepton3 lepton3(arguments.GetOption("-s")->param);
			lepton3.StartCapture();

			while(true)
			{
				int tries = 3;
				const uint8_t *frame = nullptr;

				while(tries-- >= 0 && (frame = lepton3.GetFrame(frameAcquisitionTimeout)) == nullptr)
				{
					BOOST_LOG_TRIVIAL(warning) << "Getting a frame timed out after " << frameAcquisitionTimeout << " ms.";
					lepton3.StopCapture();
					lepton3.StartCapture();
				}

				if(frame == nullptr)
				{
					BOOST_LOG_TRIVIAL(error) << "Failed obtaining a frame after 3 tries.";
					throw runtime_error("Connection with the camera has been lost.");
				}

				try
				{
					compressingStream.write(reinterpret_cast<const char*>(frame), Lepton3::FrameBufferSize);
					asio::write(socket, compressedStreamBuffer);
				}

				catch(exception &e)
				{
					BOOST_LOG_TRIVIAL(trace) << "Lost connection with the client: " << e.what();
					BOOST_LOG_TRIVIAL(info) << "Client disconnected.";
					break;
				}
			}
		}
	}

	catch (exception& e)
	{
		BOOST_LOG_TRIVIAL(fatal) << "Exception: " << e.what();
		return 2;
	}
}
